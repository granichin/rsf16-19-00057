# KuramotoSim

The Kuramoto model is able to describe a huge variety of examples of synchronization in the real world. We re-consider it through the framework of the network science and study the phenomenon of a particular interest, agent clustering. 
We assume that clusters are already recognized by some algorithm and then consider them as new variables on mesoscopic scale, which allows one to significantly reduce the dimensionality of a complicated (complex) system, thus reducing the required number of control inputs. 
In contrast to the common approach, where each agent is treated separately, we propose an alternative one using a supplementary control input, which is equal for the whole cluster. 
We also perform an analysis of this input by finding its limitations required for cluster structure to remain invariant in a network of Kuramoto oscillators. 
The theoretical results are demonstrated on a simulated multi-agent network with multiple clusters.

Check our paper: https://link.springer.com/chapter/10.1007%2F978-3-030-59535-7_35
