import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.decomposition import TruncatedSVD
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import ElasticNet

from scipy.stats import ortho_group


def gs_cofficient(v1, v2):
    return np.dot(v2, v1) / np.dot(v1, v1)


def multiply(cofficient, v):
    #     return map((lambda x : x * cofficient), v)
    return np.array(v) * cofficient


def proj(v1, v2):
    return multiply(gs_cofficient(v1, v2), v1)


def gs(X):
    X = list(X)
    Y = []
    for i in range(len(X)):
        temp_vec = X[i]
        for inY in Y:
            proj_vec = proj(inY, X[i])
            temp_vec = temp_vec - proj_vec
        Y.append(temp_vec / np.linalg.norm(temp_vec))
    return np.array(Y)


def get_Q_p_r_from_argmin(x_argmin):
    dim = int((-3 + np.sqrt(9 + 8 * (len(x_argmin) - 1))) // 2)
    r = x_argmin[0]
    x_argmin = x_argmin[1:]
    #     p = x_argmin[:dim]
    #     x_argmin = x_argmin[dim:]
    p = -1
    Q = np.zeros(shape=(dim, dim))
    for i in range(dim):
        Q[i, i:] = x_argmin[:(dim - i)]
        x_argmin = x_argmin[(dim - i):]

    Q = Q + Q.T - np.diagflat(np.diag(Q))
    return Q, p, r


def get_Q_p_r_from_argmin1(x_argmin):
    dim = int((-3 + np.sqrt(9 + 8 * len(x_argmin))) // 2)
    r = None
    #     r = x_argmin[0]
    #     x_argmin = x_argmin[1:]
    p = x_argmin[:dim]
    x_argmin = x_argmin[dim:]
    #     p = -1
    Q = np.zeros(shape=(dim, dim))
    for i in range(dim):
        Q[i, i:] = x_argmin[:(dim - i)]
        x_argmin = x_argmin[(dim - i):]

    Q = Q + Q.T  # - 2 * np.diagflat(np.diag(Q))
    #     Q *= 2.
    return Q, p, r


def get_root(Q, p, r):
    return - np.linalg.pinv(Q).dot(p)  # / 2.


def get_Q_p_r(zs, ys):
    zs_2 = PolynomialFeatures(interaction_only=True).fit_transform(zs)

    #     lin_reg = LinearRegression()
    lin_reg = ElasticNet(alpha=1, l1_ratio=.0)
    lin_reg.fit(zs_2, ys)

    return get_Q_p_r_from_argmin(lin_reg.coef_)


# def

def get_Q_p_r1(zs, ys):
    zs_2 = PolynomialFeatures(interaction_only=False, include_bias=False).fit_transform(zs)

    lin_reg = LinearRegression()
    #     lin_reg = ElasticNet(alpha=100., l1_ratio=.0)
    lin_reg.fit(zs_2, ys)

    return get_Q_p_r_from_argmin1(lin_reg.coef_)


def normalize(x):
    return x / np.sqrt(x.dot(x))


def next_x(x, P, x_optim):
    return (np.eye(d) - P.T.dot(P)).dot(x - x_optim) + x_optim


def orthogonalize(x, p):
    return x - p.dot(x) * p / p.dot(p)


def update_P(P, g):
    for i in range(1, P.shape[0]):
        g = orthogonalize(g, P[i, :])
    g = normalize(g)
    return np.vstack((P[1:, :], g))


def standardize_P(P):
    for i1 in range(P.shape[0]):
        for i2 in range(i1 + 1, P.shape[0]):
            P[i2, :] = orthogonalize(P[i2, :], P[i1, :])

    for i1 in range(P.shape[0]):
        P[i1, :] = normalize(P[i1, :])

    return P


def update_P_cg(P, g1, g0):
    #     b_k = g1.dot(g1) / g0.dot(g0)
    b_k = g1.dot(g1) / (g0).dot(g0)
    p_k = - g1 + P[-1, :] * b_k
    return np.vstack((P[1:, :], normalize(p_k)))


def update_P_cd(P, ind):
    P = np.zeros(P.shape)
    q, n = P.shape
    for i, j in enumerate(range(q * ind, q * (ind + 1))):
        P[i, j % n] = 1
    return P

