import numpy as np
import torch
from torch.optim.optimizer import Optimizer, required
import itertools
from operator import mul
from functools import reduce
from torch.optim.sgd import SGD
import random

from utils import *


_BE_RIGHT = True


def find_symmetric_hessian_mat(XT, GT):
#     XT = X.T
#     GT = G.T
    XT_ = np.linalg.pinv(XT)
    Q = XT_.dot(GT) + (np.eye(XT.shape[1]) - XT_.dot(XT)).dot((XT_.dot(GT)).T)
    return Q


class _POOptimizer:
    def __init__(self, x0, q, m, step_rate, momentum=0., restart_every=50):
        self.q = q
        self.m = m
        self.step_rate = step_rate
        self.momentum = momentum
        self.restart_every = restart_every
        self.x = x0
        self.x_trace = []
        self.g_trace = []
        self.f_trace = []
        self.P = None
        self.cnt = 0
        self.target_increase_trace = []
        self.target_increase_cnt = 0
        self._p_k = None
        self._momentum_buf = np.zeros(self.x.shape)

    def line_search(self, foo, x, d):
        s = 1. / np.sqrt(self.cnt + 1)
        foo_scalar = lambda a: foo(x + a * d)
        a_arr = np.array([-s, 0, s])
        f_arr = np.array([foo_scalar(-s), self.f_trace[-1], foo_scalar(s)])
        X = np.vstack((a_arr * a_arr, a_arr, np.ones(len(a_arr)))).T
        theta = np.linalg.pinv(X.T.dot(X)).dot(X.T).dot(f_arr)
        a_opt = - theta[1] / theta[0] / 2.
        return a_opt

    def update_P4_cg(self, P, x, g1, g0):
        if self._p_k is None:
            self._p_k = - g1
            return np.vstack((P[1:,:], normalize(self._p_k)))

        b_k = g1.dot(g1 - g0) / g0.dot(g0)
        self._p_k = - g1 + self._p_k * b_k
        return np.vstack((P[1:,:], normalize(self._p_k)))

    def _update_traces(self, *, x, f, g):
        if self.P is None:
            self.P = standardize_P(np.random.normal(0, 1, (self.q, len(x))))

        self.x_trace.append(np.copy(x))
        self.x_trace = self.x_trace[-self.m:]

        self.f_trace.append(f)
        self.f_trace = self.f_trace[-self.m:]

        self.g_trace.append(g)
        self.g_trace = self.g_trace[-self.m:]

        if len(self.x_trace) >= self.m:
            self.P = self.update_P4_cg(self.P, x, self.g_trace[-1], self.g_trace[-2] if len(self.g_trace) > 1 else None)
            if self.cnt == self.m:
                print("first time")
                self.P = self.P[-1:,:]

    def _step_gd(self, *, f, g, x, foo):
        d = g
        self._momentum_buf = self._momentum_buf * self.momentum + d
        d = self._momentum_buf
        self.x -= d * self.step_rate
        return self.x

    def step(self, *, f, g, x, foo):
        f_, g_, x_ = f, g, x
        self.cnt += 1
        self._update_traces(x=x_, f=f_, g=g_)
        if len(self.x_trace) < self.m:
            return self._step_gd(f=f_, g=g_, x=x_, foo=foo)

        if random.random() < min(np.mean([x for x in self.target_increase_trace if x != 0.5][-10:]), 0.9):
            self.target_increase_trace.append(0.5)
            return self._step_gd(f=f_, g=g_, x=x_, foo=foo)

        P = self.P
        x0 = self.x_trace[0]
        f0 = self.f_trace[0]
        f0_grad = self.g_trace[0]
        z_trace = [[0] * self.P.shape[0]]
        g_grad_trace = [[0] * self.P.shape[0]]
        g_trace = [0]
        for i in range(1, len(self.x_trace)):
            x = self.x_trace[i]
            z = P.dot(x - x0)
            f = self.f_trace[i]
            f_grad = self.g_trace[i]
            g = - ( (f - f0) - (x - x0).dot(f_grad - f0_grad) )
            g_grad = P.dot(f_grad - f0_grad)
            g_grad_trace.append(list(g_grad))
            z_trace.append(list(z))
            g_trace.append(g)

        Z = np.array(z_trace)
        G = np.array(g_grad_trace)
        y = np.array(g_trace)
#         Q, p, r = get_Q_p_r1(Z, y)
#         Q /= 2.
#         z_opt = - np.linalg.pinv(Q).dot(p)
        Q = find_symmetric_hessian_mat(Z[1:,:], G[1:,:])
        z_opt = - np.linalg.pinv(Q).dot(P.dot(self.g_trace[-1]))
        x_opt = x0 - P.T.dot(- z_opt + P.dot(x0))
        a = self.line_search(foo, self.x, x_opt - self.x)
        x_next = self.x + a * (x_opt - self.x)
        if foo(x_next) > self.f_trace[-1]:
            x_next = self._step_gd(f=f_, g=g_, x=x_, foo=foo)
            self.target_increase_trace.append(1)
            self.target_increase_cnt += 1
            if self.target_increase_cnt > 2:
                self.x_trace = []
                self.g_trace = []
                self.f_trace = []
                self.P = None
                self.target_increase_cnt = 0
        else:
            self.target_increase_trace.append(0)
            self.target_increase_cnt = 0
        if self.cnt % self.restart_every == 0:
            self.x_trace = []
            self.g_trace = []
            self.f_trace = []
            self.P = None

        self.x = x_next
        return self.x


def _mul(iterable):
    return reduce(mul, iterable, 1)


def add_to_list_limit_size(lst, elem, max_size):
    lst.append(elem)
    while len(lst) > max_size:
        lst.pop(0)
    return lst


def partial_target_fn(target_fn, param, value):
    param_data_backup = param.data
    param.data = torch.tensor(value)
    target_value = target_fn()
    param.data = param_data_backup
    return target_value


class PO(Optimizer):

    def __init__(self, params,
                 step_rate=1e-5, momentum=0.,
                 subspace_dim=1, trace_size=5,
                 target_fn=required
                 ):
        defaults = dict(
            step_rate=step_rate,
            momentum=momentum,
            subspace_dim=subspace_dim,
            trace_size=trace_size,
            target_fn=target_fn
        )
        super(PO, self).__init__(params, defaults)
        self.state["target_fn"] = target_fn

    def _gpid(self, gid, pid):
        """
        Create joint group-parameter identifier
        :param gid: group identifier
        :param pid: parameter identifier
        :return: group-parameter id
        """
        return ("gpid", gid, pid)

    def _get_param(self, gpid):
        gid = gpid[1]
        pid = gpid[2]
        return self.param_groups[gid]["params"][pid]

    def _set_param_state(self, gid, pid):
        return self.state[self._gpid(gid, pid)]

    def _global_param_get(self):
        """
        Join all parameters into a single 1d array
        :return: global parameter 1d array
        :rtype: np.ndarray
        """
        value = np.array([], dtype=np.float)
        grad_value = np.array([], dtype=np.float)
        for gid, group in enumerate(self.param_groups):
            for pid, param in enumerate(group['params']):
                if param.grad is None:
                    continue
                value = np.concatenate((value, param.data.numpy().flatten()))
                grad_value = np.concatenate((grad_value, param.grad.data.numpy().flatten()))

        return value, grad_value

    def _global_param_set(self, value):
        """
        Set global value to each parameter
        :param value: global parameter array
        :type value: np.ndarray
        """
        offset = 0
        for gid, group in enumerate(self.param_groups):
            for pid, param in enumerate(group['params']):
                if param.grad is None:
                    continue
                n = param.numel()
                param.data.set_(torch.tensor(
                    value[offset:(offset + n)].reshape(param.shape),
                    dtype=param.dtype
                ))
                offset += n

    def _global_target_fn(self, value, current_value=None):
        """
        Evaluate target function at given global parameter value
        """
        if current_value is None:
            current_value, _ = self._global_param_get()

        self._global_param_set(value)
        target_value = self.state["target_fn"]()
        self._global_param_set(current_value)
        return target_value

    def po_step_global(self):
        value, grad_value = self._global_param_get()
        assert value.shape == grad_value.shape
        target_value = self.state["f_trace"][-1]

        if "optimizer" not in self.state:
            self.state["optimizer"] = _POOptimizer(
                x0=value,
                q=self.defaults["subspace_dim"],
                m=self.defaults["trace_size"],
                step_rate=self.defaults["step_rate"],
                momentum=self.defaults["momentum"]
            )

        optimizer = self.state["optimizer"]
        next_value = optimizer.step(
            f=target_value,
            g=grad_value,
            x=value,
            foo=lambda array: self._global_target_fn(array, current_value=grad_value)
        )
        self._global_param_set(next_value)

    def step(self, target_value, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        if not self.state["f_trace"]:
            self.state["f_trace"] = []

        if target_value is not None:
            self.state["f_trace"].append(target_value)
        else:
            raise NotImplementedError()

        self.po_step_global()
        return loss

