import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import string
from random import randint

#********** CALCULATION OF THE OPTIMAL TAU VALUE *************

#*********** Material properties *****************************

E = 27.0e9
tau = 3.5e-4
alpha = 1.
sigmaC = 42.e6

#*********** Experimental data *******************************

def get_data(data_source):
    f=open(data_source,'r')
    data1 = np.array([])
    data2 = np.array([])
    while 1:
        line = f.readline()
        if not line: break
        vals = string.split(line)
        try:
            data1 = np.append(data1, float(vals[0]))
        except IndexError: continue
        except ValueError: continue
        try:
            data2 = np.append(data2, float(vals[1])) 
        except IndexError:
            data1 = np.delete(data1, len(data1)-1)
        except ValueError:
            data1 = np.delete(data1, len(data1)-1)
    f.close()
    
    return (data1, data2)

(StrainRateExp, SigmaExp) = get_data('Concrete/Concrete_SigmaC_42_17.txt')

STEQ = len(SigmaExp)

for j in range(STEQ):
    SigmaExp[j] = SigmaExp[j] * 1e6

# Input material properties 
(E, sigmaC) = get_data('Concrete/Concrete_SigmaC_42_17_Prop.txt')

#*********** Strain rate interval ****************************
NumPoints = 500

low_rate = 0.1 * min(StrainRateExp)
low_rate = 0.001
high_rate = 10. * max(StrainRateExp)
strain_rate_deg = np.linspace(np.log10(low_rate), np.log10(high_rate),NumPoints)
strain_rate = pow(10, strain_rate_deg)


#********** Set interval for search of the tau value *********

low_tau = 5e-8
high_tau = 10e-4
TauQ = 150
tauRange_deg = np.linspace(np.log10(low_tau), np.log10(high_tau), TauQ)
tauRange = pow(10, tauRange_deg)

#********* Main Function for fracture time *******************

def MyFunc1(x, k):
    if k <= 1:
        F = pow(x, alpha + 1) - k
    else:
        F = pow(x, alpha + 1) - pow(x - 1, alpha + 1) - k
    return F  

#************* Calculation of optimal tau value **************

KoefExp = np.empty((STEQ, TauQ))
for i in range(STEQ):
    for j in range(TauQ):
        KoefExp[i,j] = (alpha + 1) * pow(sigmaC / (E * StrainRateExp[i] * tauRange[j]), alpha)

SUMM = np.empty((TauQ))
indMin = 0

for j in range(TauQ):
    for i in range(STEQ):
        def LoopFunc(x):
            F = MyFunc1(x, KoefExp[i,j]) 
            return F
        if KoefExp[i,j] <= 1:
            X = pow(KoefExp[i,j], 1 / (alpha + 1))
        else:
            XGuess = sigmaC / (E * StrainRateExp[i] * tauRange[j])
            X = opt.fsolve(LoopFunc, XGuess)
        SUMM[j] = SUMM[j] + (X * tauRange[j] * E * StrainRateExp[i] - SigmaExp[i]) ** 2
    if j == 0:
        MinSUMM = SUMM[j]
    else:
        if SUMM[j] < MinSUMM:
            indMin = j
            MinSUMM = SUMM[j]
        
tau = tauRange[indMin]
tau = tauRange[indMin] * 1.9

#**************** Plot graph for tau defined above ***********

Koef = (alpha + 1) * pow(sigmaC / (E * tau * strain_rate), alpha)
sigmaD = np.array([])
XX = np.array([])
XXGuess = np.array([])

for i in range(len(Koef)):
    def LoopFunc(x):
        F = MyFunc1(x, Koef[i]) 
        return F
    if Koef[i] <= 1:
        X = pow(Koef[i], 1 / (alpha + 1))
    else:
        XGuess = sigmaC / (E * strain_rate[i] * tau)
        X = opt.fsolve(LoopFunc, XGuess)
        
    sigmaD = np.append(sigmaD, X * tau * E * strain_rate[i])

fig = plt.figure()
fig.suptitle(r'$\tau$ =' + str(tau), fontsize=14, fontweight='bold')

#ax = fig.add_subplot(111)
#fig.subplots_adjust(top=0.85)

plt.xlabel('Strain rate, 1/s')
plt.ylabel('Strength, Pa')
plt.axis([strain_rate[0], 1.05 * strain_rate[-1], 0.9 * sigmaC, max(SigmaExp)])
plt.semilogx(StrainRateExp, SigmaExp, 'bo', label='experimental data')
model_label = r'$\tau$=' + str(tau) + 's'
plt.semilogx(strain_rate, sigmaD, 'r', label = model_label)
plt.legend(loc=2)
#plt.show()
plt.savefig('Concrete/Anim/Concrete_1_9.png', format='png', dpi=1000)

#*************** Record data to the data.txt *****************

f = open('Concrete/Anim/Concrete_1_9.txt', 'w')
for i in range(NumPoints):
    line = str(strain_rate[i]) + '  ' + str(sigmaD[i]) + '\n'
    f.write(line)
f.close()

