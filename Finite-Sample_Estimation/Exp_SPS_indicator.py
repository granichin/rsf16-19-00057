import numpy as np 
import matplotlib.pyplot as plt
import scipy.optimize as opt
import string

from random import randint

#********** CALCULATION OF THE OPTIMAL TAU VALUE *************

#*********** Material properties *****************************

E = 27.0e9
tau = 5.5e-4
alpha = 15.
sigmaC = 42.5e6

#*********** Experimental data *******************************

def get_data(data_source):
    f=open(data_source,'r')
    data1 = np.array([])
    data2 = np.array([])
    while 1:
        line = f.readline()
        if not line: break
        vals = string.split(line)
        try:
            data1 = np.append(data1, float(vals[0]))
        except IndexError: continue
        except ValueError: continue
        try:
            data2 = np.append(data2, float(vals[1])) 
        except IndexError:
            data1 = np.delete(data1, len(data1)-1)
        except ValueError:
            data1 = np.delete(data1, len(data1)-1)
    f.close()
    
    return (data1, data2)

(StrainRateExp, SigmaExp) = get_data('Steel_CrNiMl/Steel_CrNiMl.txt')

N = len(SigmaExp)

for j in range(N):
    SigmaExp[j] = SigmaExp[j] * 1e6
# Input material properties 
(E, sigmaC) = get_data('Steel_CrNiMl/Steel_CrNiMl_Prop.txt')

#********** SPS Algorithm *****************************

M = 50    # Quantity of random sets (less than 2^N)
q = 5     # Confidence parameter

Beta = np.zeros([M - 1, N])
RN = np.array([])

print(N)

for j in range(M - 1):
    keyloop = 1
    while keyloop == 1:
        keyone = 1
        while keyone == 1:
            for i in range(N):
                if randint(0, 1) > 0.5:
                    Beta[j, i] = 1
                else:
                    Beta[j, i] = -1
                    keyone = 0                    
        if j != 0:
            for k in range(j):
                if str(Beta[j]) != str(Beta[k]):
                    keyloop = 0
                else:
                    keyloop = 1
                    break
        else:
            keyloop = 0
print(Beta)

#*********** Strain rate interval ****************************
NumPoints = 100

low_rate = 0.5 * min(StrainRateExp)
high_rate = 1.5 * max(StrainRateExp)

#low_rate = 1.2e-3
#high_rate = 3e1

strain_rate_deg = np.linspace(np.log10(low_rate), np.log10(high_rate),NumPoints)
strain_rate = pow(10, strain_rate_deg)

#********** Set interval for search of the tau value *********

low_tau = 3e-8
high_tau = 8e-6
TauQ = 1000
tauRange_deg = np.linspace(np.log10(low_tau), np.log10(high_tau), TauQ)
tauRange = pow(10, tauRange_deg)

#********* Main Function for fracture time *******************

#def MyFunc1(x, k):
#    if k <= 1:
#        F = pow(x, alpha + 1) - k
#    else:
#        F = pow(x, alpha + 1) - pow(x - 1, alpha + 1) - k
#    return F

def MyFunc1(x, k):
    if k < 1:
        F = x - pow(k, 1 / (alpha + 1))
    else:
#       F = x - pow(k + pow((x - 1), alpha + 1), 1 / (alpha + 1))
        F = pow(x, alpha + 1) - pow(x - 1, alpha + 1) - k
    return F

#************ Main Functions *********************************

#def Phi(k):
#    if k <= 1:
#        F = 1 + k
#    else:
#        F = 2 * pow(k, 0.5)
#    return F

def Phi(k):
    if k <= 1:
        F = 1 + k
    else:
        F = 2 * pow(k, 0.5)
    return F

#def Phi_deriv(k):
#    if k <= 1:
#        F = k
#    else:
#        F = pow(k, 0.5)
#    return F 

def Phi_deriv(x, k):
    if k < 1:
        F = x - alpha * k / ((alpha + 1) * pow(x, alpha))
    else:
        F = x - alpha * k / ((alpha + 1) * (pow(x, alpha) - pow(x - 1, alpha)))
    return F 
#************* Calculation of optimal tau value **************

tau = np.zeros(M - 1)
Rank = np.ones(TauQ)
Rank2 = np.ones(TauQ)
H0 = np.zeros(TauQ)
H = np.zeros((TauQ, M - 1))
delta = np.zeros((TauQ, N))


for k in range(TauQ):
    for j in range(M - 1):
        for i in range(N):
            Koef = (tauRange[k] * E * StrainRateExp[i]) / (2 * sigmaC)
            def LoopFunc(x):
                F = MyFunc1(x, Koef) 
                return F
            if Koef <= 1:
                X = pow(Koef, 1 / (alpha + 1))
            else:
                XGuess = sigmaC / (E * strain_rate[i] * tauRange[k])
                X = opt.fsolve(LoopFunc, XGuess)
                        
            delta[k, i] = 2 * X * Koef - SigmaExp[i] / sigmaC
            deriv = Phi_deriv(X, Koef)
            H[k, j] = H[k, j] + Beta[j, i] * delta[k, i] * deriv
            if j == 0:
                H0[k] = H0[k] + delta[k, i] * deriv
        H0[k] = abs(H0[k])
        H[k, j] = abs(H[k, j])
        if H0[k] >= H[k, j]:
            Rank[k] = Rank[k] + 1
    H[k].sort()

for k in range(TauQ - 2):
    if Rank[k + 1] <= M - q and Rank[k] > M - q:
        ind_min = k + 1
    if Rank[k + 1] <= M - q and Rank[k + 2] > M - q:
        ind_max = k + 1 
        break
print(Rank)

tau_min, tau_max = tauRange[ind_min], tauRange[ind_max]
#tau_min, tau_max = pow(10, low_tau_deg), pow(10, high_tau_deg)

    
#**************** Plot graph for tau defined above ***********

Koef = np.array([(alpha + 1) * pow(sigmaC / (E * tau_min * strain_rate), alpha),
                 (alpha + 1) * pow(sigmaC / (E * tau_max * strain_rate), alpha)])

tauMM = np.array([tau_min, tau_max])
sigmaD = np.zeros((2, NumPoints))

for j in range(2):
    for i in range(NumPoints):
        def LoopFunc(x):
            F = MyFunc1(x, Koef[j, i]) 
            return F
        if Koef[j, i] <= 1:
            X = pow(Koef[j, i], 1 / (alpha + 1))
        else:
            XGuess = sigmaC / (E * strain_rate[i] * tauMM[j])
            X = opt.fsolve(LoopFunc, XGuess)
        sigmaD[j, i] = X * tauMM[j] * E * strain_rate[i]

    

fig = plt.figure()
#fig.suptitle(r'$\tau$ =' + str(tau), fontsize=14, fontweight='bold')

#ax = fig.add_subplot(111)
#fig.subplots_adjust(top=0.85)

plt.xlabel('Strain rate, 1/s')
plt.ylabel('Strength, Pa')
plt.axis([ 0.8 * strain_rate[0], 1.05 * strain_rate[-1], 0.9* min(SigmaExp), max(SigmaExp)])
plt.semilogx(StrainRateExp, SigmaExp, 'bo',  label='experimental data')
model_label = r'$\tau\in$[' + str(tauMM[0]) + ';' + str(tauMM[1]) + ']s'
plt.semilogx(strain_rate, sigmaD[0], 'r')
plt.semilogx(strain_rate, sigmaD[1], 'r', label = model_label)
plt.legend(loc=2)
#plt.show()
plt.savefig('Steel_CrNiMl/SPS/Steel_CrNiMl_test.png', format='png', dpi=1000)

#*************** Record data to the data.txt *****************

f = open("Steel_CrNiMl/SPS/Steel_CrNiMl_test.txt","w")
for i in range(NumPoints):
    line = str(strain_rate[i]) + '  ' + str(sigmaD[0,i]) + '  ' + str(sigmaD[1,i])+ '\n'
    f.write(line)
f.close()


