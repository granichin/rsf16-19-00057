%---------------Preliminaries------------------$

n = 6; %sensors
m = 10; %targets
d = 2; %estimates dimension

Time = 1000;

alpha = 0.5 * ones(n, 1);
beta  = 0.5;
gamma = 0.5;

s = rand(d, n) * 10 + 10; %sensors placement
r = zeros(d, m, 2 * Time); %target's state
r(:, :, 1) = 100 * rand(d, m, 1);

rho = @(s, r) norm(s - r) ^ 2;
rho_bar = @(r_l, i, j_k) rho(s(:, i), r_l) - rho(s(:, j_k), r_l);
C = @(i, u, t) 2 * (s(:, u(i, 1, t)) - s(:, i))';
D = @(r_l, i, u, t) (rho_bar(r_l, i, u(i, 1, t)) + norm(s(:, u(i, 1, t))) ^ 2 - norm(s(:, i)) ^ 2);
f_t = @(i, u, t, r_hat, r_l_t) norm(pinv(C(i, u, t)) * D(r_l_t, i, u, t) - pinv(C(i, u, t)) * C(i, u, t) * r_hat) ^ 2;


%------------Randomized topology----------$
A = ones(n) - eye(n);
B = zeros(n, n, Time);
for t = 1 : Time
    for i = 1 : n
        i1_ind = randi(n - 1);
        inds = [1 : i - 1, i + 1 : n];
        i1 = inds(i1_ind);
        B(i, i1, t) = 1;
    end
end

u = zeros(n, 2, Time); 
u(:, 2, :) = randi(m, n, Time); %choice of target
for i = 1 : n
    for t = 1 : Time
        u(i, 1, t) = find(B(i, :, t)); %choice of neighbour
    end
end

%------------------Noise---------------------$
v_plus  = zeros(n, m, Time);
v_minus = zeros(n, m, Time);
for i = 1 : n
    for l = 1 : m
        for t = 1 : 10 : Time
            v_plus (i, l, t : t + 9) = (rand(1, 10) - 0.5) * rand() * 0.1;
            v_minus(i, l, t : t + 9) = (rand(1, 10) - 0.5) * rand() * 0.1;
        end
    end
end
%{
v_plus (:, :, 1) = sign(rand(n, m) - 0.5);
v_minus(:, :, 1) = sign(rand(n, m) - 0.5);
%{
for i = 1 : n
    for l = 1 : m
        for t = 1 : 50 : Time - 50
            v_plus (i, l, t : t + 49) = -1 ^ floor(t / 50);
            v_minus(i, l, t : t + 49) = -1 ^ floor(t / 50);
        end
    end
end
%}

for i = 1 : n
    for l = 1 : m
        for t = 2 : Time
            if mod(t, 50) == 1
                v_plus (i, l, t) = -1 * v_plus (i, l, t - 1);
                v_minus(i, l, t) = -1 * v_minus(i, l, t - 1);
            else
                v_plus (i, l, t) = v_plus (i, l, t - 1);
                v_minus(i, l, t) = v_minus(i, l, t - 1);
            end            
        end
    end
end
for i = 1 : n
    for l = 1 : m
        for t = 1 : Time
            v_plus (i, l, t) = v_plus (i, l, t) + 0.1 * sin(t);
            v_minus(i, l, t) = v_minus(i, l, t) + 0.1 * sin(t);
        end
    end
end
%}
%------------------Algorithm----------------------%

y = zeros(n, m, Time);
theta_hat = zeros(d, n, m, Time);
theta_hat(:, :, :, 1) = (rand(d, n, m, 1) + 1) * 50;

for t = 1 : Time - 1
    for l = 1 : m
        zeta = rand(d, 1) - 0.5;
        r(:, l, 2 * t) = r(:, l, 2 * t - 1) + zeta / norm(zeta) * 0.05; %target's movement
        r(:, l, 2 * t + 1) = r(:, l, 2 * t) + zeta / norm(zeta) * 0.05;
    end
    for i = 1 : n
        theta_hat(:, i, :, t + 1) = theta_hat(:, i, :, t) + alpha(i) * gamma * (theta_hat(:, u(i, 1, t), :, t) - theta_hat(:, i, :, t));
    end
    for i = 1 : n
        Delta = rand(d, 1) - 0.5; %simultaneous  test  perturbation
        x_2k         = theta_hat(:, i, u(i, 2, t), t) + beta * Delta;
        x_2k_minus_1 = theta_hat(:, i, u(i, 2, t), t) - beta * Delta;
        theta_hat(:, i, u(i, 2, t), t + 1) = theta_hat(:, i, u(i, 2, t), t + 1) -... 
            alpha(i) * (Delta * (f_t(i, u, t, x_2k, r(:, u(i, 2, t), 2 * t)) + v_plus(i, u(i, 2, t), t) - f_t(i, u, t, x_2k_minus_1, r(:, u(i, 2, t), 2 * t - 1)) - v_minus(i, u(i, 2, t), t)) / (2 * beta));   
    end
    for i = 1 : n
        for l = 1 : m
            y(i, l, t) = f_t(i, u, t, theta_hat(:, i, l, t), r(:, l, 2 * t)); %tracking error
        end
    end
end

%-------------------------------------Plots---------------------------------%

clr = ['r', 'g', 'b', 'c', 'm', 'k'];

%----------Tracking error-----%
figure('name', 'y')
hold on
for i = 1 : n
    for l = 1 : m
        plot(1 : Time, squeeze(y(i, l, 1 : Time)))
    end
end
hold off

%-----------------Map------------------%
figure('name', 'scene')
xlabel('x')
ylabel('y')
sampling = 50;
hold on
for l = 1 : m
    plot(squeeze(r(1, l, 1 : sampling : 2 * Time - 1)), squeeze(r(2, l, 1 : sampling : 2 * Time - 1)), '.', 'color',...
        clr(mod(l, length(clr)) + 1))
end
for i = 1 : n
    plot(s(1, i), s(2, i), 'o')
    for l = 1 : m
        plot(squeeze(theta_hat(1, i, l, 1 : sampling : Time)), squeeze(theta_hat(2, i, l, 1 : sampling : Time)), '--+', 'color', clr(mod(l, length(clr)) + 1));
    end
end
hold off